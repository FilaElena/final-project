export const Hamburger = ({ isOpen }) => {
    return (
        <>
            <div className="hamburger__wrapper">
                <div className="burger burger1"></div>
                <div className="burger burger2"></div>
                <div className="burger burger3"></div>
            </div>

            <style jsx='true'>{`
                .hamburger__wrapper {
                    width: 30px;
                    height: 30px;
                    display: flex;
                    justify-content: space-around;
                    flex-direction: column;
                    z-index: 10;
                }

                .burger {
                    width: 30px;
                    height: 3px;
                    border-radius: 10px;
                    background-color: black;
                    transform-origin: 1px;
                    transition: all 0.3s linear;
                }

                .burger1{
                    transform: ${ isOpen ? 'rotate(45deg)' : 'rotate(0)'};
                }
                .burger2{
                    transform: ${ isOpen ? 'translateX(100%)' : 'translateX(0)'};
                    opacity: ${ isOpen ? 0 : 1};
                }
                .burger3{
                    transform: ${ isOpen ? 'rotate(-45deg)' : 'rotate(0)'};
                }


            `}</style>
        </>

    )
}