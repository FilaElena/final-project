import { Link } from "react-router-dom"
import { HeartTwoTone } from '@ant-design/icons';
import { Button } from "antd";
import { favouritesStore } from "../../../common/store/FavouritesStore"; 
import { observer } from "mobx-react-lite";
import { useState } from "react";

export const PortfolioListItem = observer(({item}) => {
    const {mainPhoto, title, id} = item;
    const {addToFavourites, deleteItem} = favouritesStore

    const [itemFav, setItemFav] = useState(false);

    const handleClickList = () => {
        addToFavourites(item);
        setItemFav(!itemFav);
    }

    const handleClickListDelete = (id) => {
        deleteItem(id);
        setItemFav(!itemFav);
    }

    return (
        <li className = "main__portfolio__list__item">
            <div className = "main__portfolio__list__item__image">
                <Link to ={`/portfolio/${id}`}><img src ={mainPhoto} alt={title}/></Link>
            </div>
            <div className = "main__portfolio__list__item__info">
                <Link to ={`/portfolio/${id}`} className = "main__portfolio__list__item__link">{title}</Link>
                { itemFav === false  && <Button onClick={handleClickList} type="text" icon = {<HeartTwoTone twoToneColor="#eb2f96" style={{ fontSize: '28px'}} />} />}
                { itemFav === true && <Button onClick={() => handleClickListDelete(id)} type="text" style = {{backgroundColor: "#d2c8bd"}} icon = {<HeartTwoTone twoToneColor="#eb2f96" style={{ fontSize: '28px'}} />} />}
            </div>
        </li>
    )
})