import './ServicesPageDevelopment.css'
import '../ServicesPageFlat/ServicesPageFlat.css'
import { useNavigate} from 'react-router-dom';

export const ServicesPageDevelopment = () => {
    const navigate = useNavigate();

    const goBack = () => {
        navigate('..');
    }

    return (
        <div className='main__services__page__wrapper__wr'>

            <div className='main__services__page__wrapper__title__wr development__wrap'>
                <h1 className='main__services__page__wrapper__title'>Строительство и ремонт</h1>
            </div>

            <button className='main__services__page__btn' onClick={goBack}>Назад</button>

            <div className='main__services__page' >
                <h2 className='main__services__page__title'>Услуги</h2>
                <p className='main__services__page__text'>Наша студия оказывает услуги комплексного ремонта, мы работаем от разработки дизайн-проекта до заселения. В нашей студии работают мастера высокого уровня с многолетним опытом работы. На каждый конкретный вид работ есть отдельный высококвалифицированный специалист, курирует процесс выполнения ремонта прораб и главный инженер проекта, который имеет необходимую аттестацию.</p>
            </div>

            <div className='main__services__page__development__plan__wrap'>
                <div className='main__services__page__development__plan'>

                    <div className='main__services__page__development__plan__right'>
                        <div className='main__services__page__development__plan__item'>
                            <h2 className='main__services__page__development__plan__item__title'>Контроль качества</h2>
                            <p className='main__services__page__development__plan__item__content'>В задачи инженера входит проверка качества и технический контроль над проектом, наши клиенты общаются с инженером, который при необходимости объясняет им суть любого этапа работы, показывает качество. Все коммуникации со строителями ведет прораб, инженера и ведущий дизайнер. Коммуникации с ТЭС/ЖЭС также ведет инженера, тем самым минимизируя затраченное время клиента и освобождая его от этих забот.</p>
                        </div>
                    </div>

                    <div className='main__services__page__development__plan__left'>
                        <div className='main__services__page__development__plan__item'>
                            <h2 className='main__services__page__development__plan__item__title'>Закупка материалов</h2>
                            <p className='main__services__page__development__plan__item__content'>Закупкой и доставкой черновых и чистовых материалов занимается специалист по снабжению, который составляет графики закупки материалов, а также готовит коммерческие предложения.</p>
                        </div>

                        <div className='main__services__page__development__plan__item'>
                            <h2 className='main__services__page__development__plan__item__title'>Подробная смета</h2>
                            <p className='main__services__page__development__plan__item__content'>До начала ремонтных работ составляется подробная смета с точными объемами и расценками, после согласования сметы на работы готовится смета на черновые материалы и графики закупки.</p>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    )
}