import { NavLink, Link } from 'react-router-dom';
import { HeartOutlined } from '@ant-design/icons';
import { Logo } from "../../../common/components/Logo"
import './Header.css'
import { Hamburger } from './components/Hamburger';
import { useState } from 'react';

export const Header = () => {

    const [hamburgerOpen, setHamburgerOpen] = useState(false);

    const toggleHamburger = () =>{
        setHamburgerOpen(!hamburgerOpen)
    }

    return (
        <div className="header__wrapper">

            <Logo />

            <nav className="nav">
              <ul className="nav__items">
                <li className="nav__item"><NavLink to="/">Главная</NavLink></li>
                <li className="nav__item"><NavLink to="/aboutUs">О нас</NavLink></li>
                <li className="nav__item"><NavLink to="/services">Услуги</NavLink></li>
                <li className="nav__item"><NavLink to="/portfolio">Портфолио</NavLink></li>
                <li className="nav__item"><NavLink to="/contacts">Контакты</NavLink></li>
                <li className="nav__item favourite__icon__link">
                    <Link to="/favourites">
                        <HeartOutlined className='favourite__icon' style={{ fontSize: '35px', color: '#665961' }} />
                    </Link>
                </li>
              </ul>

              <div className='hamburger' onClick={toggleHamburger}>
                    <Hamburger isOpen={hamburgerOpen}/>
              </div>
            </nav>


            <style jsx='true'>{`

                @media (max-width: 939px){
                    .hamburger{
                        position: ${hamburgerOpen ? 'fixed' : 'static'};
                    }

                    .nav__items{
                        display: ${hamburgerOpen ? 'inline' : 'none'};
                    }
                }
    
            `}</style>

        </div>
    )
}