import { makeAutoObservable } from "mobx";

class FavouritesStore {
    favourites = JSON.parse(localStorage.getItem('favourites')) || [];

    constructor() {
        makeAutoObservable(this);
    }

    updateLocalStorage = () => {
        localStorage.setItem('favourites', JSON.stringify(this.favourites));
    }

    deleteItem = (productId) => {
        this.favourites = this.favourites.filter(({id}) => id !== productId);
        this.like = false;
        this.updateLocalStorage();
    }

    addToFavourites = (item) => {
        const findFavouriteItemIndex = this.favourites.findIndex(({id}) => id === item.id);
        if (findFavouriteItemIndex === -1) {
            this.favourites.push(item);
        } 
        this.updateLocalStorage();
    }

}

export const favouritesStore = new FavouritesStore();