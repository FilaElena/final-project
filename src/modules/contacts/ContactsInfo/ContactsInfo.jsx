import { useNavigate } from "react-router-dom"
import "./ContactsInfo.css"
import { LayOutContainer } from "../../../app/components/LayOutContainer";
import { Link } from "react-router-dom";

export const ContactsInfo = () => {
    const navigate = useNavigate();

    const handleOrderClick = () => {
        navigate('/contacts/form');
    }

    return (
        <>
            <div className="main__contacts__header__wrapper">
                <LayOutContainer>
                    <div className="main__contacts__header">
                        <h1 className="main__contacts__header__title">Свяжитесь с нами</h1>
                    </div>
                </LayOutContainer>
            </div>

            <LayOutContainer>
                <h2 className="main__contacts__title">Мы любим встречать новых людей и помогать им.</h2>

                <div className="main__contacts__wrapper">
                    
                    <div className="main__contacts__content">
                        <div className="main__contacts__content__wrapper">
                            <div className="main__contacts__content__icon"><img src={require('../images/icon-phone.png')} alt="phone" /></div>
                            <div className="main__contacts__content__text"><Link to="tel:375291555555">+375 (29) 155-55-55</Link></div>
                        </div>
                        <div className="main__contacts__content__wrapper">
                            <div className="main__contacts__content__icon"><img src={require('../images/icon-mail.png')} alt="mail" /></div>
                            <div className="main__contacts__content__text"><Link to="mailto:info@interno.com">info@interno.com</Link></div>
                        </div>
                        <div className="main__contacts__content__wrapper">
                            <div className="main__contacts__content__icon"><img src={require('../images/icon-ws.png')} alt="website" /></div>
                            <div className="main__contacts__content__text"><Link to="/">www.interno.com</Link></div>
                        </div>
                    </div>

                    <div className="main__contacts__offer">
                        <p className="main__contacts__offer__content">Уважаемые клиенты, мы работаем каждый день с 9:00 до 20:00. Если вы заинтерисованы и хотите приехать к нам, переходите по кнопке:</p>

                        <button onClick = {handleOrderClick} className="main__contacts__offer__btn">Закажите дизайн сейчас!</button>

                    </div>
            
                </div>

                <div className="main__contacts__iframe__wrapper">
                        <iframe title="Map" className="main__contacts__iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1174.9169237765805!2d27.557978638749482!3d53.91692837282362!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dbcf9086968fb9%3A0x65bd39ccb96e82a3!2z0L_RgNC-0YHQvy4g0JzQsNGI0LXRgNC-0LLQsCAyNSwg0JzQuNC90YHQug!5e0!3m2!1sru!2sby!4v1686126323645!5m2!1sru!2sby" style={{allowfullscreen:"", loading :"lazy", referrerpolicy :"no-referrer-when-downgrade"}}></iframe>
                </div>

            </LayOutContainer>

        </>
        
    )
}