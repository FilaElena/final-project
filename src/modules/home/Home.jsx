import { Button } from "antd";
import { LayOutContainer } from "../../app/components/LayOutContainer";
import { ButtonBlack } from "./components/ButtonBlack";
import { ButtonLight } from "./components/ButtonLight";
import { useNavigate } from 'react-router-dom';
import "./Home.css"

export const Home = () => {
    const navigate = useNavigate();

    const handleClickToFlat = () => {
        navigate ('./services/flat')
    }

    const handleClickToHouse = () => {
        navigate ('./services/house')
    }

    const handleClickToDevelopment = () => {
        navigate ('./services/development')
    }

    return (
        <>
            <div className="main__home__start__wrapper">
                <LayOutContainer>
                    <div className="main__home__start__info">
                        <h1 className="main__home__start__info__title">Пусть ваш дом будет уникальным</h1>
                        <p className="main__home__start__info__text">Создадим авторский дизайн интерьера и сделаем ремонт под ключ</p>
                        <ButtonBlack text = 'Начать'/>
                    </div>
                </LayOutContainer>
            </div>

            <LayOutContainer>
            <div className="main__home__services">
                <div className="main__home__service">
                    <h2 className="main__home__service__title">Дизайн интерьера квартиры</h2>
                    <p className="main__home__service__text">Разработаем для вас идеальный дизайн интерьера, который легко воплотить в жизнь.</p>
                    <Button onClick={handleClickToFlat} type="text" className="main__home__service__button" size="large" style={{fontSize: '18px', color: 'rgb(173, 111, 68)'}}>Перейти</Button>
                </div>
                <div className="main__home__service">
                    <h2 className="main__home__service__title">Дизайн интерьера дома</h2>
                    <p className="main__home__service__text">Полный комплекс услуг по реконструкции вашего дома: архитектура, дизайн интерьера и фасада.</p>
                    <Button type="text" onClick={handleClickToHouse} className="main__home__service__button" size="large" style={{fontSize: '18px', color: 'rgb(173, 111, 68)'}}>Перейти</Button>
                </div>
                <div className="main__home__service">
                    <h2 className="main__home__service__title">Строительство и ремонт</h2>
                    <p className="main__home__service__text">Выполним комплексный ремонт без Вашего участия. Работаем с собственными специалистами и со всеми документами.</p>
                    <Button type="text" onClick={handleClickToDevelopment}  className="main__home__service__button" size="large" style={{fontSize: '18px', color: 'rgb(173, 111, 68)'}}>Перейти</Button>
                </div>
            </div>
            </LayOutContainer>

            <LayOutContainer>
                <div className="main__home__opinions">
                    <h2 className="main__home__opinions__title">Что клиенты думают о нас?</h2>
                    <div className="main__home__opinions__wrapper">
                        <div className="main__home__opinion">
                            <div className="main__home__opinion__header">
                                <div className="main__home__opinion__header__photo"><img src={require('./images/opinion-nat.png')} alt="photo_nat" /></div>
                                <div className="main__home__opinion__header__name">Наталья Смитт</div>
                            </div>
                            <p className="main__home__opinion__content">Остались довольны сотрудничеством и решением, которое предложили нам дизайнеры. Благодаря дизайнерам наша квартира получилась функциональной, уютной. Здесь одинаково приятно отдыхать и работать.</p>
                        </div>
                        <div className="main__home__opinion">
                            <div className="main__home__opinion__header">
                                <div className="main__home__opinion__header__photo"><img src={require('./images/opinion-ben.png')} alt="photo_ben" /></div>
                                <div className="main__home__opinion__header__name">Бен Гринн</div>
                            </div>
                            <p className="main__home__opinion__content">У нас был ограниченный бюджет плюс хотелось поскорее переехать со съемного жилья. Я не верил, что получится сделать что-то интересное, чтобы удовлетворить потребности каждого члена семьи. Сказать, что нам все понравилось — это ничего не сказать. </p>
                        </div>
                        <div className="main__home__opinion">
                            <div className="main__home__opinion__header">
                                <div className="main__home__opinion__header__photo"><img src={require('./images/opinion-lyiz.png')} alt="photo_lyiz" /></div>
                                <div className="main__home__opinion__header__name">Луиза Йароу</div>
                            </div>
                            <p className="main__home__opinion__content">Результат превзошел наши ожидания. Мы, конечно, познакомились с портфолио на сайте, но все равно были небольшие сомнения насчет того, смогут ли дизайнеры воплотить в жизнь немного нестандартную идею. Но все вышло отлично!</p>
                        </div>
                    </div>
                </div>
            </LayOutContainer>

            <LayOutContainer>
                <div className="main__home__offer__wrapper">
                    <div className="main__home__offer">
                        <h2 className="main__home__offer__title">Давайте обсудим дизайн вашего дома!</h2>
                        <div className="main__home__offer__content">Свяжитесь с нами прямо сейчас и получайте скидку 7%!</div>
                        <ButtonLight text= 'Заказать консультацию'/>
                    </div>
                </div>

            </LayOutContainer>
        </>
    )
}