import './FavouritesForm.css'
import { Form, Input, Button, Select } from "antd"
import { useNavigate } from "react-router-dom";

const { Option } = Select;

export const FavouritesForm = () =>{

    const navigate = useNavigate();

    const handleOrderFinish = (value) => {
        navigate('../result', {
            state: value
        })
    }

    const prefixSelector = (
        <Form.Item name="prefix" noStyle>
          <Select style={{width: 110}}>
            <Option value="+37529">+37529</Option>
            <Option value="+37525">+37525</Option>
            <Option value="+37544">+37544</Option>
            <Option value="+37533">+37533</Option>
          </Select>
        </Form.Item>
    );

    return (
            <div className="favourites__form__wrapper">
                <h2 className="favourites__form__title">Заполните форму ниже и наш специлист свяжется с вами в рабочее время.</h2>
                <p className='favourites__form__content'>Мы получим варианты дизайнов, которые вам понравились из нашего портфолио. Это поможет нам лучше понимать ваши вкусы и пожелания.</p>
                <div className="favourites__form">
                    <Form onFinish={handleOrderFinish}>
                        <Form.Item
                            label="Имя"
                            name="userName"
                            rules={[
                                {
                                    required: true,
                                    message: 'Поле обязательное',
                                },
                                {
                                    min: 3,
                                    message: 'Имя слишком короткое',
                                },
                            ]}
                        >
                            <Input className="form__input" />
                        </Form.Item>

                        <Form.Item
                            label="Фамилия"
                            name="secondName"
                            rules={[
                                {
                                    required: true, 
                                    message: 'Поле обязательное',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Почта"
                            name="email"
                            rules={[
                                {
                                    type: 'email',
                                    message: 'Не валидный email',
                                },
                            ]}
                        >
                            <Input placeholder="ivanov@mail.ru"/>
                        </Form.Item>

                        <Form.Item
                            label="Телефон"
                            name="phone"
                            rules={[
                                {
                                    required: true,
                                    message: 'Пожалуйста, введите свой номер телефона',
                                },
                            ]}
                        >
                            <Input addonBefore={prefixSelector} placeholder="1112299"/>
                        </Form.Item>

                        <Form.Item 
                            label="Тип услуги"
                            name="type"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Поле обязательное',
                                },
                            ]}>
                            <Select
                                placeholder="Выберете интерисующий тип услуги"
                                allowClear
                            >               
                                <Option value="flat">Дизайн интерьера квартиры</Option>
                                <Option value="house">Дизайн интерьера дома</Option>
                                <Option value="other">Общая консультация</Option>
                            </Select>
                        </Form.Item>

                        <Form.Item
                            label="Площадь объекта (м2)"
                            name="Footage"
                            rules={[
                                {
                                    max: 4,
                                    message: 'Слишком большая площадь помещения'
                                },
                            ]}
                        >
                            <Input placeholder="55"/>
                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                                Отправить
                            </Button>
                        </Form.Item>
                    </Form>
                </div>

            </div>
    )
}