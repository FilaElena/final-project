import "./PortfolioList.css"
import { PortfolioListItem } from "../components/PortfolioListItem";
import { portfolioListStore } from "../store/PortfolioListStore"
import { Spin } from "antd";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";

export const PortfolioList = observer(() => {

    const {loading, portfolioList, loadPortfolioData} = portfolioListStore;

    useEffect(() => {
        loadPortfolioData();
    }, [loadPortfolioData])


    if (!portfolioList) {
        return (<Spin size="large" tip="Loading..."><div className="portfolio__list__spin"></div></Spin>)
    }


    return (
        <Spin spinning = {loading}>
            <div className="main__portfolio__list">
                <ul>
                    {portfolioList.map((item, index) => 
                        <PortfolioListItem item = {item} key = {index} />
                    )}
                </ul>

            </div>
        </Spin>
    )
})