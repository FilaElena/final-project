import { ContactsInfo } from "./ContactsInfo"
import { ContactsForm } from "./ContactsForm"
import { Route, Routes } from "react-router-dom"
import { ResultPage } from "./ResultPage"

export const Contacts = () => {
    return (
        <Routes>
            <Route index element={<ContactsInfo />}/>
            <Route path='/form' element={<ContactsForm />}/>
            <Route path='/result' element={<ResultPage />}/>
        </Routes>
    )
}