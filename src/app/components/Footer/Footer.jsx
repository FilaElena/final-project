import './Footer.css'
import { Logo } from "../../../common/components/Logo";
import { Link } from 'react-router-dom';
import { FacebookOutlined, InstagramOutlined, TwitterOutlined, LinkedinOutlined} from '@ant-design/icons';

export const Footer = () => {
    return (
        <div className='footer__wrapper'>

            <div className="footer__info">
                <Logo />
                <div className="footer__info__text">Лучшая студия интерьерного дизайна в Минске</div>
                <div className="footer__info__networks">
                    <Link to="#" class="footer__info__network fb">
                        <FacebookOutlined style={{ fontSize: '25px', color: '#4D5053' }}/>
                    </Link>
                    <Link to="#" class="footer__info__network inst">
                        <InstagramOutlined style={{ fontSize: '25px', color: '#4D5053' }}/>
                    </Link>
                    <Link to="#" class="footer__info__network twitter">
                        <TwitterOutlined style={{ fontSize: '25px', color: '#4D5053' }}/>
                    </Link>
                    <Link to="#" class="footer__info__network linkedin">
                        <LinkedinOutlined style={{ fontSize: '25px', color: '#4D5053' }}/>
                    </Link>
                </div>
            </div>

            <div className="footer__pages">
                <h2 className='footer__pages__title'>Карта сайта</h2>
                <ul className="footer__pages__items">
                    <li className="footer__pages__item"><Link to="/">Главная</Link></li>
                    <li className="footer__pages__item"><Link to="/aboutUs">О нас</Link></li>
                    <li className="footer__pages__item"><Link to="/services">Услуги</Link></li>
                    <li className="footer__pages__item"><Link to="/portfolio">Портфолио</Link></li>
                    <li className="footer__pages__item"><Link to="/contacts">Контакты</Link></li>
                </ul>
            </div>

            <div className='footer__coordinates'>

                <div className='footer__contacts'>
                    <h2 className='footer__contacts__title'>Контакты</h2>
                    <div className='footer__contacts__phone'><Link to="tel:375291555555">+375 (29) 155-55-55</Link></div>
                    <div class="footer__contacts__email"><Link to="mailto:info@interno.com">info@interno.com</Link></div>

                </div>

                <div className='footer__location'>
                    <h2 className='footer__location__title'>Ждем вас</h2>
                    <div class="footer__location__adress"> <Link to="https://goo.gl/maps/mat6wLmg7ZyVyebb9" target="_blank">г. Минск, просп.Машерова, 25 (3 этаж, офис 301)</Link></div>
                    <div className='footer__location__time'>c 9:00 до 21:00, без выходных</div>
                </div>

            </div>

        </div>
    )
}