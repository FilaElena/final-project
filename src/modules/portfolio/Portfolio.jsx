import './Portfolio.css'
import { LayOutContainer } from '../../app/components/LayOutContainer'
import { Route, Routes } from 'react-router-dom'
import { PortfolioList } from './PortfolioList'
import { PortfolioPage } from './PortfolioPage'

export const Portfolio = () => {
    return (
        <>
            <div className="main__portfolio__header__wrapper">
                <LayOutContainer>
                    <div className="main__portfolio__header">
                        <h1 className="main__portfolio__header__title">Портфолио</h1>
                    </div>
                </LayOutContainer>
            </div>

            <LayOutContainer>
                <Routes>
                    <Route index element = {<PortfolioList />}/>
                    <Route path = "/:itemId" element = {<PortfolioPage />}/>
                </Routes>
            </LayOutContainer>

        </>
    )
}