import './FavouritesResult.css'
import { SmileOutlined } from '@ant-design/icons';
import { Result, Button } from 'antd';
import { useLocation, useNavigate } from 'react-router-dom';

export const FavouritesResult = () => {

   const navigate = useNavigate()
   const {state} = useLocation();
   const {userName} = state;

   const handleGoHomeClick = () => {
      navigate('/');
  }

   return (
      <Result
         icon={<SmileOutlined />}
         title={`Отлично, ${userName}, ваша форма отправлена!`}
         extra={<Button onClick={handleGoHomeClick} type="primary">На главную</Button>}
      />
   )
}