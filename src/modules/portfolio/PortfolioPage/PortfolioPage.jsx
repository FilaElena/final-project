import './PortfolioPage.css'
import { useNavigate, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { Image, Button } from 'antd';
import { HeartTwoTone } from '@ant-design/icons';
import { favouritesStore } from '../../../common/store/FavouritesStore';
import { observer } from 'mobx-react-lite';
import { Spin } from 'antd';

export const PortfolioPage = observer(() => {

    const {itemId} = useParams();
    const navigate = useNavigate();

    const [itemData, setItemData] = useState(null);

    const {addToFavourites, deleteItem} = favouritesStore;

    const [liked, setLiked] = useState(false);

    const goBack = () => {
        navigate('..');
    }

    useEffect(() => {
        fetch(`http://localhost:3001/portfolio/${itemId}`)
            .then(res=>res.json())
            .then(json=>setItemData(json))
    }, [itemId]);

    const handleClickList = () => {
        addToFavourites(itemData);
        setLiked(!liked);
    }

    const handleClickListDelete = (id) => {
        deleteItem(id);
        setLiked(!liked);
    }


    if (!itemData) {
        return (<Spin size="large" tip="Loading..."><div className="portfolio__page__spin"></div></Spin>)
    }

    return (
        <div className='portfolio__page'>
            <button className='portfolio__page__goBack__btn' onClick={goBack}>К портфолио</button>
            <div className='portfolio__page__header'>
                <div className='portfolio__page__header__info__wrapper'>
                    <div className='portfolio__page__header__info'>
                        <div className='portfolio__page__header__info__content'>
                            <div className='portfolio__page__header__info__title__wrapper'>
                                <h1 className='portfolio__page__header__info__title'>{itemData.title}</h1>
                                {liked === true 
                                    ? <Button onClick={() => handleClickListDelete(itemData.id)} type="text" style = {{backgroundColor: "#d2c8bd"}} icon = {<HeartTwoTone twoToneColor="#eb2f96" style={{ fontSize: '28px'}} />} /> 
                                    : <Button onClick={handleClickList} type="text" icon = {<HeartTwoTone twoToneColor="#eb2f96" style={{ fontSize: '28px'}} />} />}
                            </div>
                            <div className='portfolio__page__header__info__footage'>Площадь: {itemData.footage}</div>
                            <div className='portfolio__page__header__info__rooms'>Комнаты: {itemData.rooms}</div>
                            <div className='portfolio__page__header__info__location'>Расположение: {itemData.location}</div>
                            <div className='portfolio__page__header__info__style'>Стиль: {itemData.style}</div>
                            <div className='portfolio__page__header__info__year'>Год: {itemData.year}</div>
                        </div> 
                    </div>
                </div>
                
                <div className='portfolio__page__header__image'><img src ={itemData.photo01} alt={itemData.title}/></div>
            </div>
            <div className='portfolio__page__task'>
                <h2 className='portfolio__page__task__title'>Задача:</h2>
                <div className='portfolio__page__task__content'>{itemData.task}</div>
            </div>
            <div className='portfolio__page__photos'>
                <div className='portfolio__page__photo'><Image src={itemData.photo02} alt={itemData.title}/></div>
                <div className='portfolio__page__photo'><Image src={itemData.photo03} alt={itemData.title}/></div>
            </div>
            <div className='portfolio__page__description'>
                <p className='portfolio__page__description__content'>{itemData.description}</p>
            </div>

        </div>
    )
})