import './ServicesPageHouse.css'
import '../ServicesPageFlat/ServicesPageFlat.css'
import { useNavigate} from 'react-router-dom';

export const ServicesPageHouse = () => {
    const navigate = useNavigate();

    const goBack = () => {
        navigate('..');
    }

    return (
        <div className='main__services__page__wrapper__wr'>
            

            <div className='main__services__page__wrapper__title__wr house__wrap'>
                <h1 className='main__services__page__wrapper__title'>Дизайн интерьера дома</h1>
            </div>

            <button className='main__services__page__btn' onClick={goBack}>Назад</button>

            <div className='main__services__page' >
                <h2 className='main__services__page__title'>Разработка дизайн-проекта</h2>
                <p className='main__services__page__text'>Студия Interno разработает дизайн интерьера дома или коттеджа и учтет пожелания каждого члена семьи. Дизайнерские решения нашей студии — это эргономичные и стильные помещения. Каждый дизайн-проект коттеджа разрабатывается индивидуально: мы учитываем количество членов семьи, увлечения, хобби и другие детали. Мы продумываем все детали и разрабатываем концепцию каждой комнаты в доме. Незаметно разместить электрику, гармонично расставить мебель, продумать уровни освещения и цветовую гамму — наши архитекторы и дизайнеры решат все вопросы.</p>
            </div>
            <div className='main__services__page__plan'>
                <h2 className='main__services__page__plan__title'>Сроки выполнения</h2>
                <div className='main__services__page__plan__sub__title'>Расчет примерный для дома 150м2</div>
                <div className='main__services__page__project'>
                    <div className='main__services__page__wrapper design__wrapp'>
                        <div className='main__services__page__header'>
                            <div className='main__services__page__title__blocks'>Дизайн-проект</div>
                            <div className='main__services__page__calc'>
                                <div className='main__services__page__terms'>от 3 месяцев</div>
                                <div className='main__services__page__price'>от 9 500 р.</div>
                            </div>
                        </div>
                        <div className='main__services__page__blocks'>
                            <ul>
                                <li className='main__services__page__item'>Сбор требований</li>
                                <li className='main__services__page__item'>Планировочная концепция</li>
                                <li className='main__services__page__item'>Визуализация основных помещений</li>
                                <li className='main__services__page__item'>Строительная документация</li>
                                <li className='main__services__page__item'>Комплектация</li>
                            </ul>
                        </div>
                    </div>

                    <div className='main__services__page__wrapper'>
                        <div className='main__services__page__header'>
                            <div className='main__services__page__title__blocks house__title'>Строительные работы</div>
                            <div className='main__services__page__calc'>
                                <div className='main__services__page__terms'>от 8 месяцев</div>
                                <div className='main__services__page__price house__price'>Стоимость рассчитывается индивидуально</div>
                            </div>
                        </div>
                        <div className='main__services__page__blocks'>
                            <ul>
                                <li className='main__services__page__item'>Подготовительные работы</li>
                                <li className='main__services__page__item'>Черновые работы</li>
                                <li className='main__services__page__item'>Чистовые работы</li>
                                <li className='main__services__page__item'>Финишная отделка</li>
                                <li className='main__services__page__item'>Техническое ведение проекта</li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    )
}