import { ArrowRightOutlined } from '@ant-design/icons';
import './ButtonBlack.css'
import { useNavigate } from 'react-router-dom';

export const ButtonBlack = ({text}) => {
    const navigate = useNavigate();
    const handleClickToServices = () => {
        navigate ('./services')
    } 
    return (
            <button onClick={handleClickToServices} className="button__black" >
                <div className="button__black__text">{text} </div>
                <ArrowRightOutlined className="button__black__arrow" style={{ fontSize: '24px', color: '#CDA274' }} />
            </button>
    )
}