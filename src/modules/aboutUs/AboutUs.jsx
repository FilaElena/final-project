import './AboutUs.css'
import { LayOutContainer } from '../../app/components/LayOutContainer'

export const AboutUs = () => {
    return (
        <>
            <div className="main__aboutus__header__wrapper">
                <LayOutContainer>
                    <div className="main__aboutus__header">
                        <h1 className="main__aboutus__header__title">О нас</h1>
                    </div>
                </LayOutContainer>
            </div>

            <LayOutContainer>
                <p className='main__aboutus__text'>Студия дизайна интерьера Interno основана в 2013 году в Минске. Мы помогаем Вам изменить жизнь к лучшему, ведь нет ничего лучше, чем комфортный уютный и любимый дом. За время нашей работы мы создали базу надежных и проверенных партнеров, тем самым предлагая Вам лучшее соотношение цены и качества.</p>
            </LayOutContainer>

            <div className='main__aboutus__info__wrapper'>
                <LayOutContainer>
                    <div className='main__aboutus__info'>
                        <div className='main__aboutus__info__item'>
                            <div className='main__aboutus__info__item__numb'>10</div>
                            <div className='main__aboutus__info__item__description'>Лет опыта</div>
                        </div>
                        <div className='main__aboutus__info__item'>
                            <div className='main__aboutus__info__item__numb'>85</div>
                            <div className='main__aboutus__info__item__description'>Успешных проектов</div>
                        </div>
                        <div className='main__aboutus__info__item'>
                            <div className='main__aboutus__info__item__numb'>15</div>
                            <div className='main__aboutus__info__item__description'>Активных проектов</div>
                        </div>
                        <div className='main__aboutus__info__item'>
                            <div className='main__aboutus__info__item__numb'>95</div>
                            <div className='main__aboutus__info__item__description'>Счастливых клиентов</div>
                        </div>
                    </div>
                </LayOutContainer>
            </div>

            <LayOutContainer>
                <div className='main__aboutus__quote__wrapper'>
                    <div className='main__aboutus__quote'>
                        <p className='main__aboutus__quote__content'>«Мы постоянно следим за безопасностью материалов, новыми трендами и стараемся быть в курсе всех новинок и технологий, а для этого постоянно посещаем международные дизайнерские выставки. Главный девиз нашей студии: „Когда любишь то, что делаешь, делаешь это хорошо!“»</p>
                        <div className='main__aboutus__quote__author'>Александр, основатель студии</div>
                    </div>
                    <div className='main__aboutus__quote__img'><img src={require('./images/aleksandr-author.png')} alt='Основатель студии'/></div>
                </div>
                
            </LayOutContainer>

        </>
    )
}