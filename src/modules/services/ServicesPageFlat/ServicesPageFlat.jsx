import './ServicesPageFlat.css'
import { useNavigate} from 'react-router-dom';

export const ServicesPageFlat = () => {
    const navigate = useNavigate();

    const goBack = () => {
        navigate('..');
    }

    return (
        <div className='main__services__page__wrapper__wr'>

            <div className='main__services__page__wrapper__title__wr flat__wrap'>
                <h1 className='main__services__page__wrapper__title'>Дизайн интерьера квартиры</h1>
            </div>

            <button className='main__services__page__btn' onClick={goBack}>Назад</button>

            <div className='main__services__page' >
                <h2 className='main__services__page__title'>Разработка дизайн-проекта</h2>
                <p className='main__services__page__text'>Составим индивидуальный проект однокомнатной квартиры с учетом метража, особенностей жилища и ваших предпочтений. Дизайнер уточнит требования и ожидания от интерьера, замеряет помещение и сделает фото.Проект будет создаваться с учетом всех ваших предпочтений, вы сможете контролировать весь процесс ремонта. Опытный мастер создаст дизайн интерьера квартиры или частного дома. Помимо красоты вы получите удобство и надежность готовой работы.</p>
            </div>
            <div className='main__services__page__plan'>
                <h2 className='main__services__page__plan__title'>Сроки выполнения</h2>
                <div className='main__services__page__plan__sub__title'>Расчет примерный для квартиры 80м2</div>
                <div className='main__services__page__project'>
                    <div className='main__services__page__wrapper design__wrapp'>
                        <div className='main__services__page__header'>
                            <div className='main__services__page__title__blocks'>Дизайн-проект</div>
                            <div className='main__services__page__calc'>
                                <div className='main__services__page__terms'>от 2 месяцев</div>
                                <div className='main__services__page__price'>от 6000 р.</div>
                            </div>
                        </div>
                        <div className='main__services__page__blocks'>
                            <ul>
                                <li className='main__services__page__item'>Сбор требований</li>
                                <li className='main__services__page__item'>Планировочная концепция</li>
                                <li className='main__services__page__item'>Визуализация основных помещений</li>
                                <li className='main__services__page__item'>Строительная документация</li>
                                <li className='main__services__page__item'>Комплектация</li>
                            </ul>
                        </div>
                    </div>

                    <div className='main__services__page__wrapper'>
                        <div className='main__services__page__header'>
                            <div className='main__services__page__title__blocks'>Строительные работы</div>
                            <div className='main__services__page__calc'>
                                <div className='main__services__page__terms'>от 4 месяцев</div>
                                <div className='main__services__page__price'>от 35 000 р.</div>
                            </div>
                        </div>
                        <div className='main__services__page__blocks'>
                            <ul>
                                <li className='main__services__page__item'>Подготовительные работы</li>
                                <li className='main__services__page__item'>Черновые работы</li>
                                <li className='main__services__page__item'>Чистовые работы</li>
                                <li className='main__services__page__item'>Финишная отделка</li>
                                <li className='main__services__page__item'>Техническое ведение проекта</li>
                            </ul>
                        </div>
                    </div>

                    
                    
                    
                </div>
            </div>
        </div>
    )
}