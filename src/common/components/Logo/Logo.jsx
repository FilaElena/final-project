import { Link } from "react-router-dom"
import "./Logo.css";

export const Logo = () => {
    return (
        <div className="logo">
            <Link to="/">
                <div className="logo__wrapper">
                    <img src="/images/Logo.png" alt="logo"/>
                    <div className="logo__title">Interno</div>
                </div>
            </Link>
        </div>
    )
}