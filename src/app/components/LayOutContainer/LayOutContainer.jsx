import './LayOutContainer.css'

export const LayOutContainer = ({children}) => {
    return (
        <div className="container">
            {children}
        </div>
    )
}