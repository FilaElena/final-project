import './FavouritesPage.css'
import { favouritesStore } from '../../../common/store/FavouritesStore'
import { FavouritesPageItem } from './components/FavouritesPageItem';
import { observer } from 'mobx-react-lite';
import { useNavigate } from 'react-router-dom';


export const FavouritesPage = observer(() => {
    const {favourites, deleteItem} = favouritesStore;
    const navigate = useNavigate();

    const handleDelete = (deleteItemId) => {
        deleteItem(deleteItemId);
    }

    const handleClickToForm = () => {
        navigate('/favourites/order');
    }

    return (
        <>
            {favourites.length === 0 && <h1 className='favourites__list__default'>В избранное пока ничего не добавлено.</h1>}
            {favourites.length > 0 && 
                <>
                    <div className="favourites__list__wrapper">
                        <ul className='favourites__list'>
                            {favourites.map((favouritesItem, index) => 
                                <FavouritesPageItem 
                                    favouritesItem = {favouritesItem}
                                    onDelete = {handleDelete} 
                                    key = {index}
                                />
                            )}
                        </ul>
                    </div>

                    <div className='favourites__list__order'>
                        <div className='favourites__list__order__content'>Основываясь на ваших предпочтениях мы создадим наилучший проект для вас!</div>
                        <button onClick={handleClickToForm} className='favourites__list__order__btn'>Заказать консультацию</button>
                    </div>
                </>
            }
        </>
    )
})