import { Carousel } from 'antd';
import './Carousel.css'

export const CarouselHome = () => {
    return (
        <div className='carousel__home'>
            <Carousel>
              <div>
                <h3><img className='carousel__img' src={require('../images/carousel-8.jpg')} alt='inter-1'/></h3>
              </div>
              <div>
                <h3><img className='carousel__img' src={require('../images/carousel-6.jpg')} alt='inter-2'/></h3>
              </div>
              <div>
                <h3><img className='carousel__img' src={require('../images/carousel-7.jpg')} alt='inter-3'/></h3>
              </div>
            </Carousel>
        </div>
      );
}