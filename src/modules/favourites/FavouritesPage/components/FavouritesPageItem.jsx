import { Button } from "antd";
import { Link } from "react-router-dom";
import {DeleteOutlined} from '@ant-design/icons';

export const FavouritesPageItem = ({favouritesItem, onDelete}) => {
    const {mainPhoto, title, id, style} = favouritesItem;

    const handleClickDeleteButton = () => {
        onDelete(id)
    }

    return (
        <li className = "favourites__list__item">
            <div className = "favourites__list__item__image">
                <Link to ={`/portfolio/${id}`}><img src ={mainPhoto} alt={title}/></Link>
            </div>
            <div className = "favourites__list__item__info">
                <div className="favourites__list__item__tittle">
                    <Link to ={`/portfolio/${id}`} className = "favourites__list__item__link">{title}</Link>
                    <Button onClick={handleClickDeleteButton} type="text" icon = {<DeleteOutlined style={{ fontSize: '20px'}} />}></Button>
                </div>
                <div className="favourites__list__item__style">Стиль: {style}</div>
            </div>
        </li>
    )
}