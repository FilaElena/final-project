import { makeAutoObservable, runInAction } from "mobx";

class PortfolioListStore {

    loading = false;
    portfolioList = undefined;

    constructor () {
        makeAutoObservable(this);
    }

    loadPortfolioData = async() => {
        this.loading = true;
        const response = await fetch(`http://localhost:3001/portfolio`);
        const json = await response.json();
        
        runInAction(() => {
            this.portfolioList = json;
            this.loading = false;
        })
    }

}

export const portfolioListStore = new PortfolioListStore()