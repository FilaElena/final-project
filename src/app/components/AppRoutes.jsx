import { Route, Routes } from "react-router-dom";
import { Home } from "../../modules/home"
import { AboutUs } from "../../modules/aboutUs"
import { Services } from "../../modules/services"
import { Portfolio } from "../../modules/portfolio"
import { Contacts } from "../../modules/contacts"
import { Favourites } from "../../modules/favourites"


export const AppRoutes = () => {
    return (
        <Routes>
            <Route index element={<Home />}/>
            <Route path='/aboutus/*' element={<AboutUs />}/>
            <Route path='/services/*' element={<Services />}/>
            <Route path='/portfolio/*' element={<Portfolio />}/>
            <Route path='/contacts/*' element={<Contacts />}/>
            <Route path='/favourites/*' element={<Favourites />}/>
        </Routes>
    )
}