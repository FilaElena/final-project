import { Link } from 'react-router-dom'
import './ServicesList.css'

export const ServicesList = () => {
    return (
        <div className='main__services__wrapper'>
                <Link to="/services/flat" className='main__services__item__link'>
                    <div className='main__services__item flat__item'>
                        <div className='main__services__item__content flat__content'>
                            <h2 className='main__services__item__title'>Дизайн интерьера квартиры</h2>
                            <p className='main__services__item__text flat__text'>Разработаем для вас идеальный дизайн интерьера, который легко воплотить в жизнь.</p>
                        </div>
                    </div>
                </Link>
               
                <Link to="/services/house" className='main__services__item__link'>
                    <div className='main__services__item house__item'>
                        <div className='main__services__item__content house__content'>
                            <h2 className='main__services__item__title'>Дизайн интерьера дома</h2>
                            <p className='main__services__item__text house__text'>Полный комплекс услуг по реконструкции вашего дома: архитектура, дизайн интерьера и фасада.</p>
                        </div>
                    </div>
                </Link>

                <Link to="/services/development" className='main__services__item__link'>
                    <div className='main__services__item development__item'>
                        <div className='main__services__item__content development__content'>
                            <h2 className='main__services__item__title'>Строительство и ремонт</h2>
                            <p className='main__services__item__text development__text'>Выполним комплексный ремонт без Вашего участия. Работаем с собственными специалистами и со всеми документами.</p>
                        </div>
                    </div>
                </Link>

           

        </div>
    )
}