import './App.css';
import { LayOutContainer } from "./components/LayOutContainer";
import { AppRoutes } from './components/AppRoutes';
import { Header } from './components/Header';
import { Footer } from './components/Footer';
import { ScrollToTop } from './utilities/ScrollToTop'


export const App = () => {
  return (
    <div className='site'>
      <header>
        <LayOutContainer>
          <Header />
        </LayOutContainer> 
      </header>

      <main>
        <AppRoutes />
      </main>

      <footer>
        <LayOutContainer>
          <Footer />
        </LayOutContainer>         
      </footer>
    
      <ScrollToTop />
    </div>
  );
}

