import { ArrowRightOutlined } from '@ant-design/icons';
import './ButtonLight.css'
import { useNavigate } from 'react-router-dom';

export const ButtonLight = ({text}) => {
    const navigate = useNavigate();
    const handleClickToServices = () => {
        navigate ('./contacts/form')
    } 
    return (
            <button onClick={handleClickToServices} className="button__light" >
                <div className="button__light__text">{text} </div>
                <ArrowRightOutlined className="button__light__arrow" style={{ fontSize: '24px', color: '#292F36' }} />
            </button>
    )
}