import './Favourites.css'
import { LayOutContainer } from '../../app/components/LayOutContainer'
import { Routes, Route } from 'react-router-dom'
import { FavouritesPage } from './FavouritesPage'
import { FavouritesForm } from './FavouritesForm'
import { FavouritesResult } from './FavouritesResult'


export const Favourites = () => {
    return (
        <>
            <div className="main__favourites__header__wrapper">
                <LayOutContainer>
                    <div className="main__favourites__header">
                        <h1 className="main__favourites__header__title">Избранное</h1>
                    </div>
                </LayOutContainer>
            </div>
            <LayOutContainer>
                <Routes>
                    <Route  index element = {<FavouritesPage/>}/>
                    <Route path="/order" element = {<FavouritesForm />}/> 
                    <Route path="/result" element = {<FavouritesResult />}/>
                </Routes>
            </LayOutContainer>
        </>
    )
}