import './Services.css'
import { LayOutContainer } from '../../app/components/LayOutContainer'
import { ServicesList } from './ServicesList'
import { ServicesPageFlat } from './ServicesPageFlat'
import { ServicesPageHouse } from './ServicesPageHouse'
import { ServicesPageDevelopment } from './ServicesPageDevelopment'
import { Route, Routes } from 'react-router-dom'

export const Services = () => {
    return (
        <>
            <div className="main__services__header__wrapper">
                <LayOutContainer>
                    <div className="main__services__header">
                        <h1 className="main__services__header__title">Услуги</h1>
                    </div>
                </LayOutContainer>
            </div>

            <LayOutContainer>
                <Routes>
                    <Route index element = {<ServicesList />}/>
                    <Route path = "/flat" element = {<ServicesPageFlat />}/>
                    <Route path = "/house" element = {<ServicesPageHouse />}/>
                    <Route path = "/development" element = {<ServicesPageDevelopment />}/>
                </Routes>
            </LayOutContainer>

        </>
    )
}