import { Result, Button} from 'antd'
import { useLocation, useNavigate } from 'react-router-dom';

export const ResultPage = () => {

    const navigate = useNavigate()
    const {state} = useLocation();
    const {userName, secondName} = state;

    const handleGoHomeClick = () => {
        navigate('/');
    }

    return (
        <Result
            status="success"
            title={`${userName} ${secondName}, ваши данные успешно отправлены!`}
            subTitle='Наш специалист вам позвонит в ближайшее время.'
            extra={[
            <Button onClick={handleGoHomeClick} type="primary" key="console">
                Главная страница
            </Button>
            ]}
        />
    )
}